package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;

public class PetTests {

	private static PetRepository petRepository;
	public static Pet pet;

	@BeforeClass
	public static void initClass() {
		Owner owner = new Owner();
		owner.setFirstName("Pablo");
		owner.setLastName("cf");
		owner.setAddress("95 N. Lake St.");
		owner.setCity("Caceres");
		owner.setTelephone("658457896");	

		pet = new Pet();
		pet.setName("Pipo");
		pet.setOwner(owner);
		//List<PetType> PetTypes=petRepository.findPetTypes();
		//pet.setType(PetTypes.get(1));
		pet.setBirthDate(LocalDate.now());
		pet.setComments("Quiere jugar a la pelota");
		pet.setWeight((float) 15.5);
		Visit visit1 = new Visit();
		visit1.setId(1);
		visit1.setDescription("Visita1");
		Visit visit2 = new Visit();
		visit2.setId(2);
		visit2.setDescription("Visita2");
		pet.getVisitsInternal().add(visit1);
		pet.getVisitsInternal().add(visit2);
	}

	@Test
	public void testPet() {
		assertNotNull(pet);				
	}

	@Test
	public void testEditVisitPet() {
		assertFalse(pet.getVisitsInternal().isEmpty());
		assertEquals(pet.getVisitsInternal().size(),2);
		assertTrue(pet.getVisits().get(1).getDescription().equals("Visita2"));
		pet.getVisits().get(1).setDescription("Visita2 editada");
		assertFalse(pet.getVisits().get(1).getDescription().equals("Visita2"));
		assertTrue(pet.getVisits().get(1).getDescription().equals("Visita2 editada"));
	}

	@AfterClass
	public static void finishClass() {
		pet = null;
	}

}
