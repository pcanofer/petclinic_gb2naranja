package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {

	@Autowired
	private VisitRepository visitRepository;

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private OwnerRepository ownerRepository;
	
	@Autowired
	private VetRepository vetRepository;

	private Owner owner;
	private Pet pet;
	private Visit visit;
	private Vet vet;
	
	@Before
	public void init() {
		owner = new Owner();
		owner.setFirstName("Pablo");
		owner.setLastName("cf");
		owner.setAddress("95 N. Lake St.");
		owner.setCity("Caceres");
		owner.setTelephone("658457896");	
		
		vet = new Vet();
		vet.setFirstName("Car");
		vet.setLastName("dc");
		vet.setHomeVisits(true);
		Collection<Specialty> vetSpecialties=vetRepository.findSpecialties();
		if(!vetSpecialties.isEmpty()) {
			for(Specialty specialty : vetSpecialties) {
				vet.addSpecialty(specialty);
			}
		}
		
		
		pet=new Pet();
		pet.setName("Pipo");
		
		List<PetType> PetTypes=petRepository.findPetTypes();
		pet.setType(PetTypes.get(0));
		pet.setBirthDate(LocalDate.now());
		pet.setComments("It's so happy");
		pet.setWeight(40.5f);
		
		visit = new Visit();
		visit.setDate(LocalDate.now());
		visit.setDescription("Descripcion1");
		visit.setVet(vet);
		owner.addPet(pet);
		
		ownerRepository.save(owner);
		
		vetRepository.save(vet);
		petRepository.save(pet);
		pet.addVisit(visit);
		vet.addVisit(visit);
		visitRepository.save(visit);
	}
	
	@Test
	@Transactional
	public void testFindById() {
		Visit visitTest = visitRepository.findById(this.visit.getId());
		assertNotNull(visitTest);
		assertEquals(visitTest,visit);
		visitTest.setDescription("Nueva descripcion");
		visitRepository.save(visitTest);
		Visit visitTest2 = visitRepository.findById(this.visit.getId());
		assertNotEquals(visitTest2.getDescription(),"Descripcion1");
		assertEquals(visitTest2.getDescription(),"Nueva descripcion");
	}


}
